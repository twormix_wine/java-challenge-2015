package test.java;

import static org.junit.Assert.*;

import org.junit.Test;

import main.java.ClientMain;

public class ServerConnectTest {

	private static final String SERVER = "http://javachallenge.loxon.hu:8443/engine/CentralControl?wsdl";
	private static final String TEAM = "twormix";
	private static final String PASSWORD = "";

	@Test
	public void test() {
		if (!ClientMain.connect(SERVER, TEAM, PASSWORD)) {
			fail("Cannot connect to server!");
		}

	}
}

package test.java;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import main.java.Sleep;

public class TurnTest {

	private long lastCalled = 0;

	private int turnsLeft;

	public boolean isMyTurn() {
		Assert.assertFalse("'isMyTurn' request too soon!",
				System.currentTimeMillis() < lastCalled + Sleep.RETRY_REQUEST_DELAY_MS);

		lastCalled = System.currentTimeMillis();
		return true;
	}

	public int getRemainingTurns() {
		return turnsLeft;
	}

	public void turn() {
		turnsLeft--;
	}

	@Before
	public void setUp() {
		turnsLeft = 5;
	}

	@Test
	public void test() {

		while (getRemainingTurns() > 0) {
			while (!isMyTurn()) {
				Sleep.SleepForMillis(Sleep.RETRY_REQUEST_DELAY_MS + 50);
			}
			turn();
			Sleep.SleepForMillis(Sleep.RETRY_REQUEST_DELAY_MS + 50);
		}
		Assert.assertFalse("Turn counter cannot be negative!", turnsLeft < 0);
	}
}

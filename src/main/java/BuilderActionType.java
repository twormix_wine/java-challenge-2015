package main.java;

public enum BuilderActionType
{
	MOVEBUILDER,
	EXPLODECELL,
	STRUCTURETUNNEL,
	RADAR,
	WATCH,
	UNKNOWN
}
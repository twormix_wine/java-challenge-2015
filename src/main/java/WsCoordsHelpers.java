package main.java;

import java.util.Random;

import main.java.generated.ObjectFactory;
import main.java.generated.WsCoordinate;
import main.java.generated.WsDirection;

public class WsCoordsHelpers {

	private ObjectFactory objf;

	private final Random rng;

	public WsCoordsHelpers(ObjectFactory objf) {
		this.objf = objf;
		rng = new Random();
	}

	public WsCoordinate convert(WsDirection dir) {
		WsCoordinate dirCord = objf.createWsCoordinate();
		dirCord.setX(0);
		dirCord.setY(0);
		switch (dir) {
		case UP:
			dirCord.setY(1);
			break;
		case DOWN:
			dirCord.setY(-1);
			break;
		case RIGHT:
			dirCord.setX(1);
			break;
		case LEFT:
			dirCord.setX(-1);
			break;
		}
		return dirCord;
	}

	public int euclideanDistance(WsCoordinate a, WsCoordinate b) {
		return Math.abs(a.getX() - b.getX()) + Math.abs(a.getY() - b.getY());
	}

	public String toString(WsCoordinate cord) {
		if (cord == null)
			return "(?,?)";
		return "(" + cord.getX() + ", " + cord.getY() + ")";
	}

	public boolean isUp(WsCoordinate cord) {
		return cord != null && cord.getX() == 0 && cord.getY() == 1;
	}

	public boolean isDown(WsCoordinate cord) {
		return cord != null && cord.getX() == 0 && cord.getY() == -1;
	}

	public boolean isLeft(WsCoordinate cord) {
		return cord != null && cord.getX() == -1 && cord.getY() == 0;
	}

	public boolean isRight(WsCoordinate cord) {
		return cord != null && cord.getX() == 1 && cord.getY() == 0;
	}

	public boolean isDir(WsCoordinate dir) {
		return isDown(dir) || isUp(dir) || isLeft(dir) || isRight(dir);
	}

	public WsDirection toDir(WsCoordinate dir) {
		if (isDown(dir)) {
			return WsDirection.DOWN;
		} else if (isUp(dir)) {
			return WsDirection.UP;
		} else if (isLeft(dir)) {
			return WsDirection.LEFT;
		} else if (isRight(dir)) {
			return WsDirection.RIGHT;
		} else {
			return null;
		}
	}

	public boolean isEquals(WsCoordinate a, WsCoordinate b) {
		return a != null && b != null && a.getX() == b.getX() && a.getY() == b.getY();
	}

	public WsCoordinate add(WsCoordinate a, WsCoordinate b) {
		WsCoordinate cord = objf.createWsCoordinate();
		cord.setX(a.getX() + b.getX());
		cord.setY(a.getY() + b.getY());
		return cord;
	}

	public WsCoordinate substract(WsCoordinate a, WsCoordinate b) {
		WsCoordinate cord = objf.createWsCoordinate();
		cord.setX(a.getX() - b.getX());
		cord.setY(a.getY() - b.getY());
		return cord;
	}

	public WsCoordinate randomize(int xMod, int yMod) {
		WsCoordinate cord = objf.createWsCoordinate();
		cord.setX(getRng().nextInt(xMod));
		cord.setY(getRng().nextInt(yMod));
		return cord;
	}

	public Random getRng() {
		return rng;
	}
}

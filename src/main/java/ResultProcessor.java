package main.java;

import java.io.PrintStream;

import main.java.generated.CommonResp;
import main.java.generated.ResultType;

public class ResultProcessor {

	public static void getResult(CommonResp resp) {
		getResult(resp, System.out);
	}

	public static void getResult(CommonResp resp, String prefix) {
		getResultPref(resp, System.out, prefix);
	}

	public static void getResultRedIfFalse(CommonResp resp) {
		getResult(resp, System.out);
		if (resp != null && resp.getType() != ResultType.DONE) {
			getResult(resp, System.err);
		}
	}

	public static void getResultRedIfFalse(CommonResp resp, String pref) {
		getResultPref(resp, System.out, pref);
		if (resp != null && resp.getType() != ResultType.DONE) {
			getResultPref(resp, System.err, pref);
		}
	}

	public static void getResult(CommonResp resp, PrintStream stream) {
		if (resp == null)
			return;
		stream.print("Unit " + resp.getBuilderUnit() + " action: " + resp.getType());
		stream.println(resp.getMessage() != null ? " - " + resp.getMessage() : "");
	}

	public static void getResultPref(CommonResp resp, PrintStream stream, String prefix) {
		if (resp == null)
			return;
		stream.print(prefix + " - ");
		getResult(resp, stream);

	}

	public static void getResultFull(CommonResp resp) {
		if (resp == null)
			return;
		if (resp.getType() == ResultType.DONE)
			getResultFull(resp, System.out);
		else
			getResultFull(resp, System.err);
	}

	public static void getResultFull(CommonResp resp, PrintStream stream) {
		if (resp == null)
			return;
		stream.print("Unit: " + resp.getBuilderUnit() + ", ");
		stream.print("Result type: " + resp.getType() + ", ");
		if (resp.getCode() != null)
			stream.print("Result code: " + resp.getCode() + ", ");
		if (resp.getMessage() != null)
			stream.print("Message: " + resp.getMessage());
		stream.println();
		stream.print("Action points left: " + resp.getActionPointsLeft() + ", ");
		stream.println("Explosives left: " + resp.getExplosivesLeft());
		stream.print("Total: " + resp.getScore().getTotal() + " = ");
		stream.print("R(" + resp.getScore().getReward() + ") + ");
		stream.print("B(" + resp.getScore().getBonus() + ") + ");
		stream.println("P(" + resp.getScore().getPenalty() + ")");
		stream.println("Turns left: " + resp.getTurnsLeft());
	}
}

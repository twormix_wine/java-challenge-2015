package main.java;

import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import main.java.generated.ObjectType;
import main.java.generated.Scouting;
import main.java.generated.WsCoordinate;
import main.java.generated.WsDirection;

public class BotStepCalculator {
	private final String team;
	private final MapData mapData;
	private final WsCoordsHelpers helper;
	private int turn = 0;

	private final Map<Integer, WsDirection> dirToMove = new HashMap<Integer, WsDirection>();
	private final Map<Integer, WsCoordinate> cellToMove = new HashMap<Integer, WsCoordinate>();
	private final Map<Integer, WsDirection> dirs = new HashMap<Integer, WsDirection>();

	public BotStepCalculator(MapData mapData, WsCoordsHelpers helper, String team) {
		this.mapData = mapData;
		this.team = team;
		this.helper = helper;
	}

	public MapData getMapData() {
		return mapData;
	}

	private boolean isCellPassable(ScoutingUpdated sc) {
		return sc == null || sc.getScouting().getObject() != ObjectType.BUILDER_UNIT
				&& sc.getScouting().getObject() != ObjectType.SHUTTLE
				&& sc.getScouting().getObject() != ObjectType.OBSIDIAN;
	}

	private BotStepType calcNextStepFromTo(WsCoordinate start, WsCoordinate stop) {

		BotStepType step = new BotStepType(null, BuilderActionType.UNKNOWN);

		if (getHelper().isEquals(start, stop))
			return step;

		int xDelta = stop.getX() - start.getX();
		int yDelta = stop.getY() - start.getY();
		WsCoordinate next = start;

		WsDirection ud = yDelta > 0 ? WsDirection.UP : WsDirection.DOWN;
		WsDirection dd = yDelta > 0 ? WsDirection.DOWN : WsDirection.UP;
		WsCoordinate uc = getHelper().convert(ud);
		WsCoordinate dc = getHelper().convert(dd);

		WsDirection rd = xDelta > 0 ? WsDirection.RIGHT : WsDirection.LEFT;
		WsCoordinate rc = getHelper().convert(rd);
		WsDirection ld = xDelta > 0 ? WsDirection.LEFT : WsDirection.RIGHT;
		WsCoordinate lc = getHelper().convert(ld);

		boolean moveUp = getHelper().getRng().nextInt(2) == 1;

		if (moveUp) {
			if (isCellPassable(getMapData().get(getHelper().add(rc, next)))) {
				next = getHelper().add(rc, next);
				step = new BotStepType(rd, BuilderActionType.MOVEBUILDER);
			} else if (isCellPassable(getMapData().get(getHelper().add(uc, next)))) {
				next = getHelper().add(uc, next);
				step = new BotStepType(ud, BuilderActionType.MOVEBUILDER);
			} else if (isCellPassable(getMapData().get(getHelper().add(dc, next)))) {
				next = getHelper().add(dc, next);
				step = new BotStepType(dd, BuilderActionType.MOVEBUILDER);
			} else if (isCellPassable(getMapData().get(getHelper().add(lc, next)))) {
				next = getHelper().add(lc, next);
				step = new BotStepType(ld, BuilderActionType.MOVEBUILDER);
			}
		} else {
			if (isCellPassable(getMapData().get(getHelper().add(uc, next)))) {
				next = getHelper().add(uc, next);
				step = new BotStepType(ud, BuilderActionType.MOVEBUILDER);
			} else if (isCellPassable(getMapData().get(getHelper().add(rc, next)))) {
				next = getHelper().add(rc, next);
				step = new BotStepType(rd, BuilderActionType.MOVEBUILDER);
			} else if (isCellPassable(getMapData().get(getHelper().add(lc, next)))) {
				next = getHelper().add(lc, next);
				step = new BotStepType(ld, BuilderActionType.MOVEBUILDER);
			} else if (isCellPassable(getMapData().get(getHelper().add(dc, next)))) {
				next = getHelper().add(dc, next);
				step = new BotStepType(dd, BuilderActionType.MOVEBUILDER);
			}
		}
		return step;
	}

	void setTurn(int turn) {
		this.turn = turn;
	}

	/*
	 * Step array, without movement steps
	 */
	BotStepType[] getBestStepsLocal(BuilderUnit unit) {

		if (!dirs.containsKey(unit.getID())) {
			WsDirection[] keys = { WsDirection.UP, WsDirection.LEFT, WsDirection.RIGHT, WsDirection.DOWN };
			dirs.put(unit.getID(), keys[unit.getID()]);
		}

		final List<BotStepType> bst = new ArrayList<BotStepType>();

		WsDirection movement = null;

		List<ScoutingUpdated> near = getMapData().getNear(unit.getCoords()).stream().filter(p -> p != null)
				.collect(Collectors.toList());

		List<ScoutingUpdated> scList;

		scList = near.stream().filter(p -> p.getScouting().getObject() == ObjectType.ROCK).collect(Collectors.toList());

		boolean foundOptimal = false;

		for (ScoutingUpdated sc : scList) {
			WsDirection dir = getHelper().toDir(getHelper().substract(sc.getScouting().getCord(), unit.getCoords()));
			if (dir.equals(dirs.get(unit.getID()))) {
				bst.add(new BotStepType(dir, BuilderActionType.STRUCTURETUNNEL));
				movement = dir;
				foundOptimal = true;
			}
		}

		if (!foundOptimal) {
			for (ScoutingUpdated sc : scList) {
				WsDirection dir = getHelper()
						.toDir(getHelper().substract(sc.getScouting().getCord(), unit.getCoords()));
				bst.add(new BotStepType(dir, BuilderActionType.STRUCTURETUNNEL));
				movement = movement == null ? dir : movement;
			}
		}

		scList = near.stream().filter(
				p -> p.getScouting().getObject() == ObjectType.TUNNEL && !p.getScouting().getTeam().equals(getTeam()))
				.collect(Collectors.toList());

		for (ScoutingUpdated sc : scList) {
			WsDirection dir = getHelper().toDir(getHelper().substract(sc.getScouting().getCord(), unit.getCoords()));
			bst.add(new BotStepType(dir, BuilderActionType.EXPLODECELL));
			bst.add(new BotStepType(dir, BuilderActionType.STRUCTURETUNNEL));
			movement = movement == null ? dir : movement;
		}

		scList = near.stream().filter(p -> p.getScouting().getObject() == ObjectType.GRANITE)
				.collect(Collectors.toList());

		for (ScoutingUpdated sc : scList) {
			WsDirection dir = getHelper().toDir(getHelper().substract(sc.getScouting().getCord(), unit.getCoords()));
			bst.add(new BotStepType(dir, BuilderActionType.STRUCTURETUNNEL));
			movement = movement == null ? dir : movement;
		}

		if (movement != null) {
			getDirToMove().put(unit.getID(), movement);
		} else {
			getDirToMove().put(unit.getID(), null);
			/*
			 * BotStepType radarstep = new BotStepType(null,
			 * BuilderActionType.RADAR); for (int i = -3; i < 3; i++) { for (int
			 * j = -3; j < 3; j++) { if (i == 0 && j == 0) continue;
			 * WsCoordinate c = new WsCoordinate();
			 * c.setX(unit.getCoords().getX() + i);
			 * c.setY(unit.getCoords().getY() + j); if (c.getX() > 0 && c.getY()
			 * > 0) radarstep.getRadarCoords().add(c); } }
			 */
		}

		return bst.toArray(new BotStepType[bst.size()]);
	}

	/*
	 * Calculate movement steps
	 */
	public BotStepType[] getBestStepMovement(BuilderUnit unit) {

		final List<BotStepType> bst = new ArrayList<BotStepType>();
		WsDirection movement = getDirToMove().getOrDefault(unit.getID(), null);

		if (movement != null
				&& isCellPassable(getMapData().get(getHelper().add(unit.getCoords(), getHelper().convert(movement))))) {
			bst.add(new BotStepType(movement, BuilderActionType.MOVEBUILDER));
			return bst.toArray(new BotStepType[bst.size()]);
		} else {
			getDirToMove().put(unit.getID(), null);
			getCellToMove().put(unit.getID(), null);
		}
		
		WsCoordinate target = getCellToMove().getOrDefault(unit.getID(), null);

		if (target != null) {
			BotStepType bs = calcNextStepFromTo(unit.getCoords(), target);
			bst.add(bs);
			getDirToMove().put(unit.getID(), bs.getDirection());
		} else {

			do {
				target = getHelper().randomize(getMapData().getSize().getX(), getMapData().getSize().getY());
			} while (getHelper().isEquals(target, unit.getCoords()));
			BotStepType bs = calcNextStepFromTo(unit.getCoords(), target);
			bst.add(bs);
			getDirToMove().put(unit.getID(), bs.getDirection());
		}
		
		return bst.toArray(new BotStepType[bst.size()]);
	}

	public String getTeam() {
		return team;
	}

	private WsCoordsHelpers getHelper() {
		return helper;
	}

	private Map<Integer, WsDirection> getDirToMove() {
		return dirToMove;
	}

	private Map<Integer, WsCoordinate> getCellToMove() {
		return cellToMove;
	}
}

package main.java;

import java.util.ArrayList;
import java.util.List;

import main.java.generated.WsCoordinate;
import main.java.generated.WsDirection;

public class BotStepType {
	private final WsDirection direction;
	private final BuilderActionType actionType;
	private final List<WsCoordinate> radarCoords = new ArrayList<WsCoordinate>();

	public BotStepType(WsDirection direction, BuilderActionType actionType) {
		this.direction = direction;
		this.actionType = actionType;
	}
	
	public WsDirection getDirection() {
		return direction;
	}

	public BuilderActionType getActionType() {
		return actionType;
	}

	public List<WsCoordinate> getRadarCoords() {
		return radarCoords;
	}
}

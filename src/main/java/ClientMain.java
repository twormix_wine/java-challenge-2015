package main.java;

import java.net.URL;

import main.java.except.IllegalOperationException;
import main.java.except.InvalidServerDataException;
import main.java.generated.*;

public class ClientMain {

	private static CentralControlServiceService ccs = null;

	public static CentralControlServiceService getCentralControlService() {
		return ccs;
	}

	public static boolean connect(String server, String team, String password) {
		try {
			java.net.Authenticator.setDefault(new java.net.Authenticator() {
				@Override
				protected java.net.PasswordAuthentication getPasswordAuthentication() {
					return new java.net.PasswordAuthentication(team, password.toCharArray());
				}
			});
			ccs = new CentralControlServiceService(new URL(server));
		} catch (Exception e) {
			ccs = null;
		}
		return ccs != null;
	}

	public static void main(String[] args) {

		if (args.length != 3) {
			System.err.println("Invalid arguments!");
			return;
		}

		final String server = args[0];
		final String team = args[1];
		final String password = args[2];

		System.out.print("Starting connection...");
		while (!connect(server, team, password)) {
			Sleep.SleepForMillis(Sleep.RETRY_CONNECTION_DELAY_MS);
		}
		System.out.println("Connected to server!");

		ObjectFactory objf = new ObjectFactory();
		WsCoordsHelpers helper = new WsCoordsHelpers(objf);
		MapData mapData = new MapData();
		BotStepCalculator calc = new BotStepCalculator(mapData, helper, team);
		SpaceController spaceShuttle = new SpaceControllerBot(getCentralControlService().getCentralControlPort(), objf);
		Player player = new PlayerBot(spaceShuttle, calc, mapData, objf, helper, team);

		if (!player.startGame())
			return;

		while (player.getRemainingTurns() > 0) {
			while (!player.isMyTurn() && !player.isGameOver()) {
				Sleep.SleepForMillis(Sleep.RETRY_REQUEST_DELAY_MS);
			}
			
			if (player.isGameOver())
				break;
			
			try {
				player.turn();
			} catch (IllegalOperationException e) {
				e.printMessage();
				e.printStackTrace();
			} catch (InvalidServerDataException e) {
				System.err.println(e.getMessage());
				e.printStackTrace();
			} catch (Exception e) {
				System.err.println(e.getMessage());
				e.printStackTrace();
			}
			Sleep.SleepForMillis(Sleep.RETRY_REQUEST_DELAY_MS);
		}

		player.printResults();
		
		System.out.println("Game over!");
	}
}

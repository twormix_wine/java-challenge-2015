package main.java;

import main.java.except.IllegalOperationException;
import main.java.except.InvalidServerDataException;

public interface Player {
	boolean startGame();

	boolean isGameOver();

	boolean isMyTurn();

	void turn() throws IllegalOperationException, InvalidServerDataException;

	int getRemainingTurns();
	
	void printResults();
}

package main.java.except;

public class InvalidServerDataException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public InvalidServerDataException(String message){
        super(message);
    }
}

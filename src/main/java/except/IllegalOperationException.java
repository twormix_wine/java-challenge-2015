package main.java.except;

import java.util.ArrayList;
import java.util.List;

import javax.swing.plaf.synth.SynthSeparatorUI;

import main.java.BuilderActionType;
import main.java.ResultProcessor;
import main.java.generated.CommonResp;

public class IllegalOperationException extends Exception {
	private static final long serialVersionUID = 1L;

	private final BuilderActionType opType;
	private final CommonResp failingResult;

	private final List<BuilderActionType> chainActions = new ArrayList<BuilderActionType>();

	public void addCauseActions(List<BuilderActionType> opType) {
		chainActions.addAll(opType);
	}

	public IllegalOperationException(IllegalOperationException e, BuilderActionType op) {
		super(e);
		this.opType = op;
		this.addCauseActions(e.getChainActions());
		this.failingResult = e.getFailingResult();
	}

	public IllegalOperationException(CommonResp fail, BuilderActionType op) {
		super();
		this.opType = op;
		this.failingResult = fail;
	}

	public IllegalOperationException(CommonResp fail, BuilderActionType op, String message) {
		super(message);
		this.opType = op;
		this.failingResult = fail;
	}

	public BuilderActionType getOpType() {
		return opType;
	}

	public CommonResp getFailingResult() {
		return failingResult;
	}

	public List<BuilderActionType> getChainActions() {
		return chainActions;
	}

	public void printMessage() {
		System.err.println("Error during action " + opType);
		for (BuilderActionType at : getChainActions()) {
			System.err.println(" - due to " + at);
		}
		System.err.println("Result: ");
		ResultProcessor.getResultFull(getFailingResult());
		System.err.println();
	}
}

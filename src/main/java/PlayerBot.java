package main.java;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import main.java.except.IllegalOperationException;
import main.java.except.InvalidServerDataException;
import main.java.generated.ActionCostResponse;
import main.java.generated.CommonResp;
import main.java.generated.IsMyTurnResponse;
import main.java.generated.ObjectFactory;
import main.java.generated.ObjectType;
import main.java.generated.RadarResponse;
import main.java.generated.ResultType;
import main.java.generated.Scouting;
import main.java.generated.StartGameResponse;
import main.java.generated.WatchResponse;
import main.java.generated.WsBuilderunit;
import main.java.generated.WsCoordinate;
import main.java.generated.WsDirection;

public class PlayerBot implements Player {

	private final String team;

	private final MapData mapData;
	private final BotStepCalculator calc;
	private final SpaceController controller;
	private final ObjectFactory objf;
	private final WsCoordsHelpers helper;

	private final Map<Integer, BuilderUnit> units;
	private Integer currentUnitID = null;
	private int remainingTurns;
	private int elapsedTurns;
	private ActionCostResponse actionCosts;
	private int actionPointsLeft;
	private int explosivesLeft;

	private boolean canTurn = false;

	public PlayerBot(SpaceController spaceShuttle, BotStepCalculator calc, MapData mapData, ObjectFactory objf,
			WsCoordsHelpers helper, String team) {
		this.units = new HashMap<>();
		this.controller = spaceShuttle;
		this.objf = objf;
		this.mapData = mapData;
		this.helper = helper;
		this.team = team;
		this.calc = calc;
	}

	@Override
	public boolean startGame() {
		StartGameResponse resp = getController().startGame(getObjf().createStartGameRequest());
		if (resp.getResult().getType() == ResultType.DONE) {
			getMapData().setSize(resp.getSize());
			for (WsBuilderunit unit : resp.getUnits()) {
				getUnits().put(unit.getUnitid(),
						BuilderUnitFactory.create(getController(), getObjf(), unit, getHelper(), getTeam()));
			}
		} else {
			System.err.println("Failed to start the game!");
			return false;
		}
		setRemainingTurns(resp.getResult().getTurnsLeft());
		setElapsedTurns(75 - remainingTurns);
		return true;
	}

	@Override
	public boolean isGameOver() {
		return getRemainingTurns() <= 0;
	}

	@Override
	public void printResults() {
		ResultProcessor.getResultFull(getController().getLastResult(), System.out);
	}

	@Override
	public boolean isMyTurn() {
		IsMyTurnResponse resp = getController().isMyTurn();
		if (resp.isIsYourTurn()
				&& (getCurrentUnitID() == null || getCurrentUnitID() != resp.getResult().getBuilderUnit())) {
			setCurrentUnitID(resp.getResult().getBuilderUnit());
			setRemainingTurns(resp.getResult().getTurnsLeft());
			setActionPointsLeft(resp.getResult().getActionPointsLeft());
			setExplosivesLeft(resp.getResult().getExplosivesLeft());
			setCanTurn(resp.getResult().getTurnsLeft() > 0);
		}
		return isCanTurn();
	}

	private void setRemainingTurns(int turnsLeft) {
		remainingTurns = turnsLeft;
		setElapsedTurns(75 - remainingTurns);
		calc.setTurn(elapsedTurns);
	}

	public ActionCostResponse calcActionCosts() {
		return getController().getActionCost();
	}

	private boolean explodeCell(WsCoordinate cell) throws IllegalOperationException {
		if (cell == null || getActionPointsLeft() <= 0 || !isCanTurn())
			return false;

		watchUnit();

		BuilderUnit bunit = getCurrentBuilderUnit();

		WsDirection dir = getHelper().toDir(getHelper().substract(cell, bunit.getCoords()));
		Scouting scouting = getMapData().get(cell).getScouting();

		CommonResp result = null;

		if (bunit.canExplodeCell(scouting) && getActionPointsLeft() >= getActionCosts().getExplode()
				&& getExplosivesLeft() > 0) {
			result = bunit.explodeCell(dir, cell).getResult();
		}
		if (result != null && result.getType() != ResultType.DONE)
			throw new IllegalOperationException(result, BuilderActionType.EXPLODECELL,
					"Builder " + currentUnitID + " explode cell failed!");

		if (result != null) {
			setActionPointsLeft(getActionPointsLeft() - getActionCosts().getExplode());
			setExplosivesLeft(getExplosivesLeft() - 1);
			watchUnit();
		}

		return result != null;
	}

	private boolean structureTunnel(WsCoordinate cell) throws IllegalOperationException {
		if (cell == null || getActionPointsLeft() <= 0 || !isCanTurn())
			return false;

		watchUnit();

		BuilderUnit bunit = getCurrentBuilderUnit();

		WsDirection dir = getHelper().toDir(getHelper().substract(cell, bunit.getCoords()));
		Scouting scouting = getMapData().get(cell).getScouting();

		CommonResp result = null;

		if (bunit.canStructureTunnel(scouting) && getActionPointsLeft() >= getActionCosts().getDrill()) {
			result = bunit.structureTunnel(dir, cell).getResult();
		}
		if (result != null && result.getType() != ResultType.DONE) {
			throw new IllegalOperationException(result, BuilderActionType.STRUCTURETUNNEL,
					"Builder " + currentUnitID + " structure tunnel failed!");
		}

		if (result != null) {
			setActionPointsLeft(getActionPointsLeft() - getActionCosts().getDrill());
			watchUnit();
		}

		return result != null;
	}

	private boolean structureTunnelChainActions(WsCoordinate cell) throws IllegalOperationException {
		if (cell == null)
			return false;

		try {
			if (structureTunnel(cell)) {
				return true;
			} else if (explodeCell(cell)) {
				return structureTunnel(cell);
			}
		} catch (IllegalOperationException e) {
			throw new IllegalOperationException(e, BuilderActionType.STRUCTURETUNNEL);
		}

		return false;
	}

	private boolean moveToCell(WsCoordinate cell) throws IllegalOperationException {
		if (cell == null || getActionPointsLeft() <= 0 || !isCanTurn())
			return false;

		watchUnit();

		BuilderUnit bunit = getCurrentBuilderUnit();

		WsDirection dir = getHelper().toDir(getHelper().substract(cell, bunit.getCoords()));
		Scouting scouting = getMapData().get(cell).getScouting();

		CommonResp result = null;

		if (bunit.canMove(scouting) && getActionPointsLeft() >= getActionCosts().getMove()) {
			result = bunit.moveBuilder(dir).getResult();
		}

		if (result != null && result.getType() != ResultType.DONE) {
			throw new IllegalOperationException(result, BuilderActionType.MOVEBUILDER,
					"Builder " + currentUnitID + " move failed!");
		}

		if (result != null) {
			setActionPointsLeft(getActionPointsLeft() - getActionCosts().getMove());
			watchUnit();
		}

		return result != null;
	}

	private boolean moveToCellChainActions(WsCoordinate cell) throws IllegalOperationException {
		if (cell == null)
			return false;

		try {
			if (moveToCell(cell)) {
				return true;
			} else if (structureTunnelChainActions(cell)) {
				return moveToCell(cell);
			}
		} catch (IllegalOperationException e) {
			throw new IllegalOperationException(e, BuilderActionType.MOVEBUILDER);
		}

		return false;
	}

	private boolean exitIfInside() throws IllegalOperationException, InvalidServerDataException {
		WsCoordinate spaceShuttleCoords = getController().getCoords();
		WsCoordinate exitCoords = getController().getExitCoords();
		if (getHelper().isEquals(spaceShuttleCoords, getCurrentBuilderUnit().getCoords())) {

			System.out.println("Space shuttle coords: " + getHelper().toString(spaceShuttleCoords));
			System.out.println("Space shuttle exit: " + getHelper().toString(exitCoords));

			WsCoordinate exitDir = getHelper().substract(exitCoords, spaceShuttleCoords);
			if (!getHelper().isDir(exitDir))
				throw new InvalidServerDataException("Invalid shuttle exit coord!");
			return moveToCellChainActions(exitCoords);
		}
		return true;
	}

	private boolean moveIfAtDock() throws IllegalOperationException {
		BuilderUnit bunit = getCurrentBuilderUnit();
		if (!getHelper().isEquals(bunit.getCoords(), getController().getExitCoords()))
			return true;
		List<ScoutingUpdated> scouting = getMapData().getNear(bunit.getCoords());
		for (ScoutingUpdated sc : scouting) {
			if (moveToCell(sc.getScouting().getCord()))
				return true;
		}
		for (ScoutingUpdated sc : scouting) {
			if (moveToCellChainActions(sc.getScouting().getCord()))
				return true;
		}
		return false;
	}

	private boolean watchUnit() throws IllegalOperationException {
		BuilderUnit bunit = getCurrentBuilderUnit();

		if (getActionPointsLeft() <= 0 || !isCanTurn())
			return false;

		if (getActionPointsLeft() >= getActionCosts().getWatch()) {
			WatchResponse resp = bunit.watch();

			if (resp.getResult().getType() != ResultType.DONE) {
				throw new IllegalOperationException(resp.getResult(), BuilderActionType.WATCH,
						"Builder could not C#! " + resp.getResult().getCode());
			}

			if (resp.getResult().getType() == ResultType.DONE) {
				for (Scouting scouting : resp.getScout()) {
					getMapData().update(scouting, getRemainingTurns());
				}
				return true;
			}
		}
		return false;
	}

	private boolean radar(List<WsCoordinate> coords) {
		if (coords == null)
			return false;

		if (getActionPointsLeft() <= 0 || !isCanTurn())
			return false;

		final List<WsCoordinate> filtered = new ArrayList<WsCoordinate>();

		int apsLeft = getActionPointsLeft();

		for (WsCoordinate cord : coords) {
			if (apsLeft < getActionCosts().getRadar())
				break;
			if (getMapData().get(cord) == null) {
				filtered.add(cord);
				apsLeft -= getActionCosts().getRadar();
			}
		}

		BuilderUnit bunit = getCurrentBuilderUnit();
		RadarResponse resp = bunit.radar(filtered);
		if (resp.getResult().getType() == ResultType.DONE) {
			for (Scouting scouting : resp.getScout()) {
				getMapData().update(scouting, getRemainingTurns());
			}
			setActionPointsLeft(apsLeft);
			return true;
		}
		return false;
	}

	private void addUnitToMapData() {

		Scouting sc = getObjf().createScouting();
		sc.setCord(getCurrentBuilderUnit().getCoords());
		sc.setObject(ObjectType.BUILDER_UNIT);
		sc.setTeam(getTeam());
		getMapData().update(sc, getRemainingTurns());

	}

	private void processSteps() throws IllegalOperationException {
		boolean done = false;
		do {
			done = false;
			BotStepType[] types = getCalc().getBestStepsLocal(getCurrentBuilderUnit());
			for (BotStepType type : types) {
				done |= calculateNextStep(type);
			}
			types = getCalc().getBestStepMovement(getCurrentBuilderUnit());
			for (BotStepType type : types) {
				done |= calculateNextStep(type);
			}
		} while (done);
	}

	private boolean calculateNextStep(BotStepType type) throws IllegalOperationException {

		if (type == null || !isCanTurn() || getActionPointsLeft() <= 0)
			return false;

		boolean done = false;

		WsCoordinate cord = getHelper().add(getCurrentBuilderUnit().getCoords(),
				getHelper().convert(type.getDirection()));

		switch (type.getActionType()) {
		case EXPLODECELL:
			done = explodeCell(cord);
			break;
		case MOVEBUILDER:
			done = moveToCellChainActions(cord);
			break;
		case RADAR:
			done = radar(type.getRadarCoords());
			break;
		case STRUCTURETUNNEL:
			done = structureTunnelChainActions(cord);
			break;
		default:
			break;
		}

		if (done) {
			System.out.println("APs left: " + getActionPointsLeft());
		}

		return done;
	}

	private void processBestTurn() throws IllegalOperationException, InvalidServerDataException {
		if (!exitIfInside()) {
			return;
		}

		if (!moveIfAtDock())
			return;

		processSteps();
	}

	@Override
	public void turn() throws IllegalOperationException, InvalidServerDataException {

		try {
			if (isCanTurn()) {
				System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

				System.out.println("Turns left: " + getRemainingTurns());

				setActionCosts(calcActionCosts());

				System.out.println("Processing turn for unit " + getCurrentUnitID());

				System.out.println("Action points: " + getActionPointsLeft());

				if (watchUnit()) {
					// System.out.println("Unit is at " +
					// getHelper().toString(getCurrentBuilderUnit().getCoords()));
					processBestTurn();
				}

				addUnitToMapData();
			}
		} finally {
			System.out.println("-------------------------------------");
			ResultProcessor.getResultFull(getController().getLastResult(), System.out);
			System.out.println("-------------------------------------");
			System.out.println();
			setCanTurn(false);
		}
	}

	@Override
	public int getRemainingTurns() {
		return remainingTurns;
	}

	public Integer getCurrentUnitID() {
		return currentUnitID;
	}

	private void setCurrentUnitID(Integer currentUnitID) {
		this.currentUnitID = currentUnitID;
	}

	private MapData getMapData() {
		return mapData;
	}

	public BuilderUnit getCurrentBuilderUnit() {
		if (!getUnits().containsKey(getCurrentUnitID())) {
			WsBuilderunit builder = getObjf().createWsBuilderunit();
			builder.setUnitid(getCurrentUnitID());
			BuilderUnit buint = BuilderUnitFactory.create(getController(), getObjf(), builder, getHelper(), getTeam());
			getUnits().put(getCurrentUnitID(), buint);
		}
		return getUnits().get(getCurrentUnitID());
	}

	private void setActionPointsLeft(int aps) {
		actionPointsLeft = aps;
	}

	public int getActionPointsLeft() {
		return actionPointsLeft;
	}

	public ActionCostResponse getActionCosts() {
		if (actionCosts == null)
			setActionCosts(getController().getActionCost());
		return actionCosts;
	}

	public void setActionCosts(ActionCostResponse actionCosts) {
		this.actionCosts = actionCosts;
	}

	public boolean isCanTurn() {
		return canTurn;
	}

	public void setCanTurn(boolean canTurn) {
		this.canTurn = canTurn;
	}

	private SpaceController getController() {
		return controller;
	}

	private ObjectFactory getObjf() {
		return objf;
	}

	private WsCoordsHelpers getHelper() {
		return helper;
	}

	private Map<Integer, BuilderUnit> getUnits() {
		return units;
	}

	private BotStepCalculator getCalc() {
		return calc;
	}

	private String getTeam() {
		return team;
	}

	public int getExplosivesLeft() {
		return explosivesLeft;
	}

	private void setExplosivesLeft(int explosivesLeft) {
		this.explosivesLeft = explosivesLeft;
	}

	public int getElapsedTurns() {
		return elapsedTurns;
	}

	public void setElapsedTurns(int elapsedTurns) {
		this.elapsedTurns = elapsedTurns;
	}
}

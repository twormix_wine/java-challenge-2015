package main.java;

import java.util.List;

import main.java.generated.ActionCostResponse;
import main.java.generated.ExplodeCellResponse;
import main.java.generated.MoveBuilderUnitResponse;
import main.java.generated.RadarResponse;
import main.java.generated.Scouting;
import main.java.generated.StructureTunnelResponse;
import main.java.generated.WatchResponse;
import main.java.generated.WsCoordinate;
import main.java.generated.WsDirection;

public interface BuilderUnit {

	String getTeam();

	WsCoordinate getCoords();

	WatchResponse watch();

	MoveBuilderUnitResponse moveBuilder(WsDirection dir);

	ExplodeCellResponse explodeCell(WsDirection dir, WsCoordinate explodeCellOut);

	StructureTunnelResponse structureTunnel(WsDirection dir, WsCoordinate structureCellOut);

	RadarResponse radar(List<WsCoordinate> coords);

	boolean canExplodeCell(Scouting target);

	boolean canMove(Scouting target);

	boolean canStructureTunnel(Scouting target);
	
	int getID();
}

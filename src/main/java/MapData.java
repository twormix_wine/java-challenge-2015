package main.java;

import main.java.generated.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class ScoutingUpdated {
	private Scouting scouting;
	private int lastUpdated;

	public ScoutingUpdated(Scouting scouting, int lastUpdated) {
		this.setScouting(scouting);
		this.setLastUpdated(lastUpdated);
	}

	public Scouting getScouting() {
		return scouting;
	}

	public void setScouting(Scouting scouting) {
		this.scouting = scouting;
	}

	public int getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(int lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
}

public class MapData {

	private Map<Key2D, ScoutingUpdated> data = new HashMap<Key2D, ScoutingUpdated>();
	private WsCoordinate size;

	public void update(Scouting scouting, int turn) {
		data.put(new Key2D(scouting.getCord().getX(), scouting.getCord().getY()), new ScoutingUpdated(scouting, turn));
	}

	public ScoutingUpdated get(WsCoordinate coord) {
		return data.getOrDefault(new Key2D(coord.getX(), coord.getY()), null);
	}

	public List<ScoutingUpdated> getAll() {
		return data.values().stream().collect(Collectors.toList());
	}

	public List<ScoutingUpdated> getNear(WsCoordinate coord) {
		List<ScoutingUpdated> ret = new ArrayList<ScoutingUpdated>();
		if (coord == null)
			return ret;
		WsCoordinate[] coords = new WsCoordinate[4];

		coords[0] = new WsCoordinate();
		coords[0].setX(coord.getX());
		coords[0].setY(coord.getY() + 1);

		coords[1] = new WsCoordinate();
		coords[1].setX(coord.getX());
		coords[1].setY(coord.getY() - 1);

		coords[2] = new WsCoordinate();
		coords[2].setX(coord.getX() + 1);
		coords[2].setY(coord.getY());

		coords[3] = new WsCoordinate();
		coords[3].setX(coord.getX() - 1);
		coords[3].setY(coord.getY());

		for (WsCoordinate ncoord : coords) {
			ScoutingUpdated scouting = get(ncoord);
			if (scouting != null)
				ret.add(scouting);
		}
		return ret;
	}

	public WsCoordinate getNearestObjectTo(WsCoordsHelpers helper, WsCoordinate cord, ObjectType type, String team) {
		WsCoordinate nearest = null;
		int min = 0;
		for (ScoutingUpdated sc : data.values()) {
			int len = helper.euclideanDistance(sc.getScouting().getCord(), cord);
			if (nearest == null || min == 0 || len < min && len != 0 && sc.getScouting().getObject() == type) {

				if ((type == ObjectType.TUNNEL || type == ObjectType.SHUTTLE || type == ObjectType.BUILDER_UNIT)
						&& team != null) {
					if (!team.equals(sc.getScouting().getTeam()))
						continue;
				}

				min = len;
				nearest = sc.getScouting().getCord();
			}
		}
		return nearest;
	}

	public WsCoordinate getSize() {
		return size;
	}

	public void setSize(WsCoordinate size) {
		this.size = size;
	}

}

package main.java;

import main.java.generated.ActionCostResponse;
import main.java.generated.CentralControl;
import main.java.generated.CommonResp;
import main.java.generated.ExplodeCellRequest;
import main.java.generated.ExplodeCellResponse;
import main.java.generated.GetSpaceShuttleExitPosResponse;
import main.java.generated.GetSpaceShuttlePosResponse;
import main.java.generated.IsMyTurnResponse;
import main.java.generated.MoveBuilderUnitRequest;
import main.java.generated.MoveBuilderUnitResponse;
import main.java.generated.RadarRequest;
import main.java.generated.RadarResponse;
import main.java.generated.ResultType;
import main.java.generated.StartGameRequest;
import main.java.generated.StartGameResponse;
import main.java.generated.StructureTunnelRequest;
import main.java.generated.StructureTunnelResponse;
import main.java.generated.WatchRequest;
import main.java.generated.WatchResponse;
import main.java.generated.WsCoordinate;

/*
 * proxy-like class for controller
 * */
public class SpaceControllerBot implements SpaceController {

	private main.java.generated.ObjectFactory objf;
	private CentralControl centralControl;
	private WsCoordinate coords;
	private WsCoordinate exitCoords;
	private CommonResp lastResult = null;

	public SpaceControllerBot(CentralControl centralControl, main.java.generated.ObjectFactory objf) {
		this.centralControl = centralControl;
		this.objf = objf;
	}

	@Override
	public StartGameResponse startGame(StartGameRequest parameters) {
		StartGameResponse r = centralControl.startGame(parameters);
		ResultProcessor.getResultRedIfFalse(r.getResult(), "startGame");
		lastResult = r.getResult();
		return r;
	}

	/*
	 * Gets the coordinates of the spaceship
	 */
	@Override
	public WsCoordinate getCoords() {
		CommonResp resp = null;
		try {
			if (coords == null) {
				GetSpaceShuttlePosResponse spresp = centralControl
						.getSpaceShuttlePos(objf.createGetSpaceShuttlePosRequest());
				resp = spresp.getResult();
				lastResult = spresp.getResult();
				ResultProcessor.getResultRedIfFalse(resp, "getCoords");
				coords = spresp.getCord();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return coords = null;
		}
		return coords;
	}

	/*
	 * Gets the coordinates of the spaceship exit dock
	 */
	@Override
	public WsCoordinate getExitCoords() {
		CommonResp resp = null;
		try {
			if (exitCoords == null) {
				GetSpaceShuttleExitPosResponse spresp = centralControl
						.getSpaceShuttleExitPos(objf.createGetSpaceShuttleExitPosRequest());
				resp = spresp.getResult();
				lastResult = spresp.getResult();
				ResultProcessor.getResultRedIfFalse(resp, "exitCoords");
				exitCoords = spresp.getCord();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return exitCoords = null;
		}
		return exitCoords;
	}

	@Override
	public RadarResponse radar(RadarRequest params) {
		RadarResponse r = centralControl.radar(params);
		//ResultProcessor.getResultRedIfFalse(r.getResult(), "radar");
		lastResult = r.getResult();
		return r;
	}

	@Override
	public WatchResponse watch(WatchRequest params) {
		WatchResponse r = centralControl.watch(params);
		//ResultProcessor.getResultRedIfFalse(r.getResult(), "watch");
		lastResult = r.getResult();
		return r;
	}

	@Override
	public ExplodeCellResponse explodeCell(ExplodeCellRequest params) {
		ExplodeCellResponse r = centralControl.explodeCell(params);
		//ResultProcessor.getResultRedIfFalse(r.getResult(), "explodeCell");
		lastResult = r.getResult();
		return r;
	}

	@Override
	public MoveBuilderUnitResponse moveBuilderUnit(MoveBuilderUnitRequest params) {

		MoveBuilderUnitResponse r = centralControl.moveBuilderUnit(params);
		//ResultProcessor.getResultRedIfFalse(r.getResult(), "moveBuilderUnit");
		lastResult = r.getResult();
		return r;
	}

	@Override
	public StructureTunnelResponse structureTunnelRequest(StructureTunnelRequest params) {
		StructureTunnelResponse r = centralControl.structureTunnel(params);
		//ResultProcessor.getResultRedIfFalse(r.getResult(), "structureTunnel");
		lastResult = r.getResult();
		return r;
	}

	@Override
	public IsMyTurnResponse isMyTurn() {
		IsMyTurnResponse r = centralControl.isMyTurn(objf.createIsMyTurnRequest());
		//if (r.getResult().getType() != ResultType.DONE)
		//	ResultProcessor.getResultRedIfFalse(r.getResult(), "isMyTurn");
		lastResult = r.getResult();
		return r;
	}

	@Override
	public ActionCostResponse getActionCost() {
		ActionCostResponse r = centralControl.getActionCost(objf.createActionCostRequest());
		//if (r.getResult().getType() != ResultType.DONE)
		//	ResultProcessor.getResultRedIfFalse(r.getResult());
		lastResult = r.getResult();
		return r;
	}

	@Override
	public CommonResp getLastResult() {
		return lastResult;
	}
}

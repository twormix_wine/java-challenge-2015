package main.java;

import java.util.concurrent.TimeUnit;

public class Sleep {

	public static final int RETRY_REQUEST_DELAY_MS = 170;
	public static final int RETRY_CONNECTION_DELAY_MS = 500;

	public static void SleepForMillis(int millis) {
		try {
			TimeUnit.MILLISECONDS.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}

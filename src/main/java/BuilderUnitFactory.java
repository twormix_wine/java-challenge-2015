package main.java;

import java.util.List;

import main.java.generated.ActionCostResponse;
import main.java.generated.ExplodeCellRequest;
import main.java.generated.ExplodeCellResponse;
import main.java.generated.MoveBuilderUnitRequest;
import main.java.generated.MoveBuilderUnitResponse;
import main.java.generated.ObjectFactory;
import main.java.generated.ObjectType;
import main.java.generated.RadarRequest;
import main.java.generated.RadarResponse;
import main.java.generated.ResultType;
import main.java.generated.Scouting;
import main.java.generated.StructureTunnelRequest;
import main.java.generated.StructureTunnelResponse;
import main.java.generated.WatchRequest;
import main.java.generated.WatchResponse;
import main.java.generated.WsBuilderunit;
import main.java.generated.WsCoordinate;
import main.java.generated.WsDirection;

public class BuilderUnitFactory {
	public static BuilderUnit create(SpaceController controller, ObjectFactory objf, WsBuilderunit builder,
			WsCoordsHelpers helper, String teamName) {
		return new BuilderUnitBot(controller, objf, builder, helper, teamName);
	}
}

class BuilderUnitBot implements BuilderUnit {
	private final SpaceController controller;
	private final WsBuilderunit builder;
	private final ObjectFactory objf;
	private final WsCoordsHelpers helper;
	private final String teamName;
	private final int unitID;

	BuilderUnitBot(SpaceController controller, ObjectFactory objf, WsBuilderunit builder, WsCoordsHelpers helper,
			String teamName) {
		this.helper = helper;
		this.controller = controller;
		this.objf = objf;
		this.builder = builder;
		this.teamName = teamName;
		this.unitID = builder.getUnitid();
	}

	public MoveBuilderUnitResponse moveBuilder(WsDirection dir) {
		if (dir == null)
			return null;
		MoveBuilderUnitRequest req = getObjf().createMoveBuilderUnitRequest();
		req.setDirection(dir);
		req.setUnit(getBuilder().getUnitid());
		MoveBuilderUnitResponse resp = getController().moveBuilderUnit(req);
		if (resp.getResult().getType() == ResultType.DONE) {
			WsCoordinate dirCord = helper.convert(dir);
			getBuilder().getCord().setX(getBuilder().getCord().getX() + dirCord.getX());
			getBuilder().getCord().setY(getBuilder().getCord().getY() + dirCord.getY());
			System.out.print("Bot " + getBuilder().getUnitid() + " moved to ");
			System.out.println(helper.toString(getBuilder().getCord()));
		} else {
			System.err.println("Error moving bot " + getBuilder().getUnitid());
		}
		return resp;
	}

	public ExplodeCellResponse explodeCell(WsDirection dir, WsCoordinate explodedCellOut) {
		if (dir == null)
			return null;
		ExplodeCellRequest req = getObjf().createExplodeCellRequest();
		req.setDirection(dir);
		req.setUnit(getBuilder().getUnitid());
		ExplodeCellResponse resp = getController().explodeCell(req);
		if (resp.getResult().getType() == ResultType.DONE) {
			WsCoordinate dirCord = helper.convert(dir);
			if (explodedCellOut == null)
				explodedCellOut = objf.createWsCoordinate();
			explodedCellOut.setX(getBuilder().getCord().getX() + dirCord.getX());
			explodedCellOut.setY(getBuilder().getCord().getY() + dirCord.getY());
			System.out.print("Bot " + getBuilder().getUnitid() + " bombed cell ");
			System.out.println(helper.toString(explodedCellOut));
		} else {
			System.err.println("Error exploding cell; bot id: " + getBuilder().getUnitid());
		}
		return resp;
	}

	@Override
	public RadarResponse radar(List<WsCoordinate> coords) {
		if (coords == null)
			return null;
		RadarRequest req = getObjf().createRadarRequest();
		req.getCord().addAll(coords);
		req.setUnit(getBuilder().getUnitid());
		RadarResponse resp = getController().radar(req);
		if (resp.getResult().getType() == ResultType.DONE) {
			System.out.print("Bot " + getBuilder().getUnitid() + " used the radar");
		} else {
			System.err.println("Error using the radar; bot id: " + getBuilder().getUnitid());
		}
		return resp;
	}

	@Override
	public StructureTunnelResponse structureTunnel(WsDirection dir, WsCoordinate structureCellOut) {
		if (dir == null)
			return null;
		StructureTunnelRequest req = getObjf().createStructureTunnelRequest();
		req.setDirection(dir);
		req.setUnit(getBuilder().getUnitid());
		StructureTunnelResponse resp = getController().structureTunnelRequest(req);
		if (resp.getResult().getType() == ResultType.DONE) {
			WsCoordinate dirCord = helper.convert(dir);
			if (structureCellOut == null)
				structureCellOut = objf.createWsCoordinate();
			structureCellOut.setX(getBuilder().getCord().getX() + dirCord.getX());
			structureCellOut.setY(getBuilder().getCord().getY() + dirCord.getY());
			System.out.print("Bot " + getBuilder().getUnitid() + " structured tunnel at ");
			System.out.println(helper.toString(structureCellOut));
		} else {
			System.err.println("Error structuring tunnel; bot id: " + getBuilder().getUnitid());
		}
		return resp;
	}

	private void calcPosFromScouting(List<Scouting> scouting) {
		int x = 0;
		int y = 0;
		WsCoordinate pos = objf.createWsCoordinate();
		for (Scouting scout : scouting) {
			x += scout.getCord().getX();
			y += scout.getCord().getY();
		}
		pos.setX(x / 4);
		pos.setY(y / 4);
		getBuilder().setCord(pos);
	}

	@Override
	public WatchResponse watch() {
		WatchRequest req = getObjf().createWatchRequest();
		req.setUnit(getBuilder().getUnitid());
		WatchResponse resp = getController().watch(req);
		if (resp.getResult().getType() == ResultType.DONE) {
		//	for (Scouting sc : resp.getScout()) {
		//		System.out.print(sc.getObject() + " at " + helper.toString(sc.getCord()));
		//		System.out.println(sc.getTeam() == null ? "" : ", team: " + sc.getTeam());
		//	}
			calcPosFromScouting(resp.getScout());
		} else {
			System.err.println("Bot " + getBuilder().getUnitid() + " did NOT C#.");
		}
		return resp;
	}

	public SpaceController getController() {
		return controller;
	}

	public WsBuilderunit getBuilder() {
		return builder;
	}

	public ObjectFactory getObjf() {
		return objf;
	}

	@Override
	public WsCoordinate getCoords() {
		return getBuilder().getCord();
	}

	@Override
	public boolean canMove(Scouting scouting) {
		if (scouting == null)
			return false;
		WsCoordinate dir = helper.substract(scouting.getCord(), getCoords());
		if (helper.isDir(dir)) {
			ObjectType otype = scouting.getObject();
			return otype == ObjectType.TUNNEL && scouting.getTeam().equals(getTeam());
		}
		return false;
	}

	@Override
	public boolean canExplodeCell(Scouting scouting) {
		if (scouting == null)
			return false;
		WsCoordinate dir = helper.substract(scouting.getCord(), getCoords());
		if (helper.isDir(dir)) {
			ObjectType otype = scouting.getObject();
			return (otype == ObjectType.TUNNEL) && !scouting.getTeam().equals(getTeam())
					|| (otype == ObjectType.GRANITE);
		}
		return false;
	}

	@Override
	public boolean canStructureTunnel(Scouting scouting) {
		if (scouting == null)
			return false;
		WsCoordinate dir = helper.substract(scouting.getCord(), getCoords());
		if (helper.isDir(dir)) {
			return scouting.getObject() == ObjectType.ROCK;
		}
		return false;
	}

	@Override
	public String getTeam() {
		return teamName;
	}

	@Override
	public int getID() {
		return unitID;
	}
}

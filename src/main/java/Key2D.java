package main.java;

class Key2D {
	public Key2D(int x, int y) {
		this.x = x;
		this.y = y;
	}

	final int x;
	final int y;

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj == this)
			return true;
		if (obj.getClass() != this.getClass())
			return false;
		Key2D k2 = (Key2D) obj;
		if (k2.x == this.x && k2.y == this.y)
			return true;
		return false;
	}

	@Override
	public int hashCode() {
		return x + y;
	}
}
package main.java;

import main.java.generated.*;

public interface SpaceController {
	StartGameResponse startGame(StartGameRequest params);

	WsCoordinate getCoords();

	WsCoordinate getExitCoords();

	RadarResponse radar(RadarRequest params);

	ExplodeCellResponse explodeCell(ExplodeCellRequest params);

	MoveBuilderUnitResponse moveBuilderUnit(MoveBuilderUnitRequest params);

	IsMyTurnResponse isMyTurn();

	ActionCostResponse getActionCost();

	WatchResponse watch(WatchRequest params);

	StructureTunnelResponse structureTunnelRequest(StructureTunnelRequest params);

	CommonResp getLastResult();
}
